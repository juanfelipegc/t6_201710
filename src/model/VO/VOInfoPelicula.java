package model.VO;

public class VOInfoPelicula {

	private String	titulo;
	private int	anio;
	private String	director;
	private  String	actores;
	private double	ratingIMDB;
	
	public String getTitulo(){return titulo;}
	public int getAnio(){return anio;}
	public String getDirector() {return director;}
	public String getActores(){ return actores;}
	public double getRating() {return ratingIMDB;}
	
	public void setTitulo(String pTitulo){titulo=pTitulo;}
	public void setAnio(int a){anio=a;}
	public void setDirector(String d){director=d;}
	public void setActores(String a){actores=a;}
	public void setRating(double r){ratingIMDB=r;}
	
	public int hashCode()
	{
		String msj=titulo+anio+director+actores+ratingIMDB;
		int hash=0;
		for(int i=0;i<msj.length();i++)
		{
			hash=(hash+(int)msj.charAt(i))%31;
		}
		return hash;
	}
	
	
}
