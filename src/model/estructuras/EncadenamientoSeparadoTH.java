package model.estructuras;

import java.util.Iterator;

import api.IEncadenamientoSeparadoTH;
import model.logic.GeneradorDatos;


//En esta clase cambi� todos los objetos Object por sus respectivos K y V, as� es gen�rico del todo.
public class EncadenamientoSeparadoTH<K,V> implements IEncadenamientoSeparadoTH<K,V> {

	private ListaLlaveValorSecuencial<K,V>[] listas;
	private NodoDoble<K,V> nodo;
	private int size;
	public EncadenamientoSeparadoTH(int m)
	{
		size=m;
		listas= (ListaLlaveValorSecuencial<K,V>[]) new ListaLlaveValorSecuencial[m];
		nodo= new NodoDoble<K,V>(null,null);
		
	}
	@Override
	public int darTamanio() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean estaVacia() {
		// TODO Auto-generated method stub
		return size==0;
	}

	@Override
	public boolean tieneLlave(K llave) {
		if(llave==null)return false;
			
		return !listas[hash((K)llave)].estaVacia();
	}

	@Override
	public V darValor(K llave) {
		if(llave==null) return null;
		return listas[hash(llave)].darValor(llave);
	}

	@Override
	public void insertar(K llave, V valor) {

		if(llave==null) return;
		ListaLlaveValorSecuencial<K,V> x=listas[hash((K)llave)];
		if(x==null)
		{
			listas[hash((K)llave)]= new ListaLlaveValorSecuencial<K,V>();
		}
		listas[hash((K)llave)].insertar(llave, valor);
		NodoDoble<K,V> temp= new NodoDoble<K,V>((V)valor,(K)llave);
		NodoDoble<K,V> current=nodo;
		while(current.getNext()!=null)
		{
			if(current.getNext().giveKey()==(K)llave)return;
			current=current.getNext();
		}
		current.setNext(temp);
		
	}

	@Override
	public Iterable<K> llaves() {
		// TODO Auto-generated method stub
		return (Iterable<K>)(nodo.getNext());
	}

	@Override
	public int[] darLongitudListas() {
		int[] longitud= new int[size];
		for(int i=0;i<listas.length;i++)
		{
			if(listas[i]==null) longitud[i]=0;
			else longitud[i]=listas[i].darTamanio();
		}
		return longitud;
	}
	
	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7FFFFFFF)%size;
	}

	
	
	public static void main(String[] args) {
		EncadenamientoSeparadoTH<String,Integer> l= new EncadenamientoSeparadoTH<String,Integer>(10);
		String[] datos=null;
		Integer[] valores=null;
		int[] val=new int[3];
		val[0]=1000;
		val[1]=5000;
		val[2]=10000;
		int valor=0;
		long inicio=0;
		long fin=0;
		for(int n=0;n<3;n++)
		{
			valor=val[n];
			System.out.println("Prueba con N "+valor);
			valores=GeneradorDatos.generarAgnos(valor);
			for(int k=10;k<=100;k+=10)
			{
				System.out.println("Para k = "+k);
				datos=GeneradorDatos.generarCadenas(valor, k);
				inicio=System.currentTimeMillis();
				for(int i=0;i<valor;i++)
				{
					l.insertar(datos[i], valores[i]);
				}
				fin=System.currentTimeMillis();
				System.out.println(fin-inicio);
				l= new EncadenamientoSeparadoTH<String,Integer>(10);
			}
		}
		
	}
}
