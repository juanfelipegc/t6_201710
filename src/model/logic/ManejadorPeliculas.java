package model.logic;

import java.io.*;

import com.google.gson.stream.JsonReader;

import model.VO.VOInfoPelicula;
import model.estructuras.EncadenamientoSeparadoTH;


public class ManejadorPeliculas {
	
	private EncadenamientoSeparadoTH<Long, VOInfoPelicula> tabla;
	public final static String RUTA = "./data/links_json.json";
	
	public ManejadorPeliculas(){
		tabla = new EncadenamientoSeparadoTH<Long, VOInfoPelicula>(10);
	}
	
	public String consultarTituloID(long k){
		if(tabla.darValor(k)==null) return null;
		return tabla.darValor(k).getTitulo();
	}
	
	public int consultarAnioID(long k){
		return tabla.darValor(k).getAnio();
	}
	
	public String consultarDirectorID(long k){
		return tabla.darValor(k).getDirector();
	}
	
	public String consultarActoresID(long k){
		return tabla.darValor(k).getActores();
	}
	
	public double consultarRatingID(long k){
		return tabla.darValor(k).getRating();
	}
	
	public EncadenamientoSeparadoTH<Long, VOInfoPelicula> darTabla(){
		return tabla;
	}

	public void importJson(String pRuta){
        BufferedReader in;
        try {
            in = new BufferedReader(new FileReader(new File(pRuta)));
            JsonReader reader = new JsonReader(in);
            reader.beginArray();
            VOInfoPelicula nuevo = null;
            String name = null;
            long movieId;
            String otherName = null;
            String st = null;
            while (reader.hasNext()) {
                reader.beginObject();
                nuevo = new VOInfoPelicula();
                while (reader.hasNext()) {
                    name = reader.nextName();
                    if (name.equals("movieId")) {
                        movieId = Long.parseLong(reader.nextString());
                        tabla.insertar(movieId, nuevo);
                    } else if (name.equals("imdbData")) {
                        reader.beginObject();
 
                        while (reader.hasNext()) {
                            otherName = reader.nextName();
                            if (otherName.equals("Title")) {
                                nuevo.setTitulo(reader.nextString());
                            } else if (otherName.equals("Year")) {
                                st = reader.nextString();
                                try {
                                    nuevo.setAnio(Integer.parseInt(st));
                                } catch (NumberFormatException e) {
                                    nuevo.setAnio(-1);
                                }
                            } else if (otherName.equals("Director")) {
                                nuevo.setDirector(reader.nextString());
                            } else if (otherName.equals("Actors")) {
                                nuevo.setActores(reader.nextString());
                            } else if (otherName.equals("imdbRating")) {
                                try {
                                    nuevo.setRating(Double.parseDouble(reader.nextString()));
                                } catch (NumberFormatException e) {
                                    nuevo.setRating(-1);
                                }
                            } else {
                                reader.skipValue();
                            }
                        }
                        reader.endObject();
                    } else {
                        reader.skipValue();
                    }
 
                }
                reader.endObject();
            }
            reader.endArray();
            reader.close();
           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(tabla.darTamanio());
    }
	
	public static void main(String[] args)
	{
		ManejadorPeliculas m = new ManejadorPeliculas();
		m.importJson(RUTA);
		
		
	}
	
}
