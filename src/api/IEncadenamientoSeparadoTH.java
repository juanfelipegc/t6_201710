package api;

public interface IEncadenamientoSeparadoTH<K,V> {

	
	public int darTamanio();
	public boolean estaVacia();
	public boolean tieneLlave(K llave);
	public V darValor(K llave);
	public void insertar(K llave, V valor);
	public Iterable<K> llaves();
	public int[] darLongitudListas();
}
