package api;

public interface IListaLlaveValorSecuencial<K, V> {

	public int darTamanio();
	boolean estaVacia();
	boolean tieneLlave(K llave) ;
	public V darValor(K llave) ;
	public void insertar(K llave, V valor) ;
	public Iterable<K> llaves();
}
