
 
import junit.framework.TestCase;
import model.logic.ManejadorPeliculas;
 
public class TestManejadorPeliculas extends TestCase{
 
    public void testTotalConsultas(){
 
        ManejadorPeliculas m = new ManejadorPeliculas();
        m.importJson(ManejadorPeliculas.RUTA);
        long start = getTimestampActual()/1000000;
 
        for(long i=0; i<164977;i++){
            System.out.println(i);
            m.consultarTituloID(i);
 
        }
 
        long stop = getTimestampActual()/1000000;
        long total = stop - start;
        System.out.println("se demora: " + total + "ms en consultar TODOS los elementos");
    }
 
    public void testPromedioConsultas(){
 
        ManejadorPeliculas m = new ManejadorPeliculas();
        long start2 = getTimestampActual()/1000000;
        m.importJson(ManejadorPeliculas.RUTA);
        long stop2 = getTimestampActual()/1000000;
        long total2 = stop2 - start2;
        System.out.println("se demora: " + total2 + "ms cargando el JSON");
        double promedio = 0;
        int i = 0;
        for(long j = 0;j<164977;j++){
            long start = getTimestampActual();
            m.consultarTituloID(j);
            long stop = getTimestampActual();
            long total = stop - start;
            promedio = Math.round(((promedio * (i) + total )/(i+1))*100.0)/100.0;
 
 
            i++;
        }
        System.out.println("se demora: " + promedio + " Nano-Segundos en promedio cada busqueda");
    }
 
    public void testDistribucionHash()
    {
        ManejadorPeliculas m = new ManejadorPeliculas();
        m.importJson(ManejadorPeliculas.RUTA);
        int[] distribucion = m.darTabla().darLongitudListas();
 
        System.out.print("Distribución de longitudes de la tabla: ");
 
        for (int i = 0; i < distribucion.length; i++) {
            System.out.print(distribucion[i]+ " ");
        }
    }
 
    public long getTimestampActual(){
        return System.nanoTime();
    }
 
}