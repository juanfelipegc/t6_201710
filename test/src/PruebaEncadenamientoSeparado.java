import junit.framework.TestCase;
import model.estructuras.EncadenamientoSeparadoTH;
import model.logic.GeneradorDatos;

public class PruebaEncadenamientoSeparado extends TestCase{

	private EncadenamientoSeparadoTH<String,Integer> hash;
	
	private void setupEscenario1()
	{
		hash= new EncadenamientoSeparadoTH<String,Integer>(2);
	}
	
	public void testAddNull()
	{
		long inicio=System.currentTimeMillis();
		setupEscenario1();
		hash.insertar(null, 23);
		assertFalse(hash.tieneLlave(null));
		long fin=System.currentTimeMillis();
		System.out.println("P1 "+(fin-inicio));
	}
	
	public void testAddRandom()
	{
		long inicio=System.currentTimeMillis();
		setupEscenario1();
		String[] cadenas= GeneradorDatos.generarCadenas(100, 100);
		Integer[] valor=GeneradorDatos.generarAgnos(100);
		for(int i=0;i<cadenas.length;i++)
		{
			hash.insertar(cadenas[i], valor[i]);
		}
		for(int i=0;i<cadenas.length;i++)
		{
			assertTrue("La tabla no tiene la llave requerida-"+i,hash.tieneLlave(cadenas[i]));
		}
		long fin=System.currentTimeMillis();
		System.out.println("P2 "+(fin-inicio));

	}
	
	public void testAddSame()
	{
		long inicio=System.currentTimeMillis();
		setupEscenario1();
		String unico="S";
		Integer[] valor= GeneradorDatos.generarAgnos(100);
		for(int i=0;i<valor.length;i++)
		{
			hash.insertar(unico, valor[i]);
		}
		int[] longitud=hash.darLongitudListas();
		int cantidadLlena=0;
		for(int i=0;i<longitud.length;i++)
		{
			cantidadLlena+=longitud[i];
		}
		assertTrue("La tabla no tiene la llave requerida "+unico,hash.tieneLlave(unico));
		assertTrue("El tamaño solo debería ser de 1 y fue de "+cantidadLlena,cantidadLlena==1);
		assertEquals("No se agregó el último elemento que se esperaba "+valor[valor.length-1],valor[valor.length-1],hash.darValor(unico));
		long fin=System.currentTimeMillis();
		System.out.println("P3 "+(fin-inicio));


	}
	
}
